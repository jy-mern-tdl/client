import React, { useContext } from "react"
import { Navbar, Nav } from 'react-bootstrap'
import { Link } from 'react-router-dom'

import UserContext from '../UserContext';

export default function AppNavbar(){

	const { user } = useContext(UserContext)

	return(
		<Navbar bg="danger" variant="dark" expand="lg">
		  <Link className="navbar-brand" to="/">To-Do List</Link>
		  <Navbar.Toggle aria-controls="basic-navbar-nav" />
		  <Navbar.Collapse id="basic-navbar-nav">
		    <Nav className="ml-auto">
				<Link className="nav-link active" to="/about">About</Link>
				{user.id !== null
					? 
                    <React.Fragment>
                        <Link className="nav-link active" to="/">{user.username}'s Tasks</Link>
                        <Link className="nav-link active" to="/logout">Log Out</Link>
                    </React.Fragment>
					:
					<React.Fragment>
						<Link className="nav-link active" to="/">Log In</Link>
						<Link className="nav-link active" to="/register">Register</Link>
					</React.Fragment>
				}		    	
		    </Nav>
		  </Navbar.Collapse>
		</Navbar>
	)
}
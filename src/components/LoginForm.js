import React, { useState, useContext } from 'react';
import { Form, Button, Card, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function LoginForm({fetchData}){

	const { user, setUser } = useContext(UserContext)
	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")

	const authenticate = (e) => {
		e.preventDefault()

		fetch(`${ process.env.REACT_APP_API_URL }/users/login`, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			if(typeof data.access !== 'undefined'){
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)
			}else{
				Swal.fire(
					'Error',
					'Authentication failed. Please check your login details and try again.',
					'error'
				)
			}

		})
	}
	
	const retrieveUserDetails = (token) => {

		fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
			headers: { Authorization: `Bearer ${ token }`}
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				console.log(data)
				setUser({
					username: data.username,
					id: data.uid
				  })
                fetchData();
				Swal.fire(
					'Success',
					'You have been logged in.',
					'success'
				)	
			}else{
				Swal.fire(
					'Error',
					'Authentication failed. Please check your login details and try again.',
					'error'
				)				
			}
		})
	}

	return(
		<Row className="justify-content-center">
			<Col xs md="6">
				<h2 className="text-center my-4">Log In</h2>
				<Card>
					<Form onSubmit={e => authenticate(e)}>
						<Card.Body>
							<Form.Group controlId="userEmail">
								<Form.Label>Email:</Form.Label>
								<Form.Control type="email" placeholder="Enter your email" value={email} onChange={e => setEmail(e.target.value)} required/>
							</Form.Group>

							<Form.Group controlId="password">
								<Form.Label>Password:</Form.Label>
								<Form.Control type="password" placeholder="Enter your password" value={password} onChange={e => setPassword(e.target.value)} required/>
							</Form.Group>
						</Card.Body>
						<Card.Footer>
							<Button variant="primary" type="submit" block>Submit</Button>
						</Card.Footer>
					</Form>
				</Card>
				<p className="text-center mt-3">Don't have an account yet? <Link to="/register">Click here</Link> to register.</p>
			</Col>				
		</Row>
	)
}
import { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import About from './pages/About';
import Register from './pages/Register';
import Logout from './pages/Logout';
import { UserProvider } from './UserContext';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';

function App() {
  const [user, setUser] = useState({
    username: null,
		id: null
	})

  useEffect(() => {
		fetch(`${ process.env.REACT_APP_API_URL }/users/details`, {
			headers: { Authorization: `Bearer ${ localStorage.getItem('token') }`}
		})
		.then(res => res.json())
		.then(data => {

			if(typeof data.uid !== 'undefined'){
				setUser({
          username: data.username,
          id: data.uid
        })
			}else{
				setUser({ id: null})
			}
		})

	}, [])

  return (
    <UserProvider value={{user, setUser}}>
      <Router>
        <AppNavbar/>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/about" component={About}/>
          <Route exact path="/register" component={Register}/>
          <Route exact path="/logout" component={Logout}/>
        </Switch>
      </Router>
    </UserProvider>
  );
}

export default App;

import { useState, useEffect } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import { Link, Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register(){

    const [username, setUsername] = useState("")
	const [email, setEmail] = useState("")
	const [password1, setPassword1] = useState("")
	const [password2, setPassword2] = useState("")
	const [error, setError] = useState(true)
	const [isActive, setIsActive] = useState(false)
	const [willRedirect, setWillRedirect] = useState(false)

	useEffect(() => {
		if(username === '' || email === '' || password1 === '' || password2 === ''){
			setError(true)
			setIsActive(false)
		}else if((username !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 !== password2 || password1.length < 6 || password2.length < 6)){
			setError(false)
			setIsActive(false)
		}else if((username !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2 && password1.length >= 6 && password2.length >= 6)){
			setError(false)
			setIsActive(true)
		}
	}, [username, email, password1, password2])



	const registerUser = (e) => {
		e.preventDefault()

		fetch(`${ process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({
                username: username,
				email: email,
				password: password1
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire(
					'Success',
					'Registration successful. You may now log in.',
					'success'
				)	
				setWillRedirect(true)
			}else{
				Swal.fire(
					'Error',
					'Something went wrong.',
					'error'
				)
                setUsername("")
				setEmail("")
				setPassword1("")
				setPassword2("")
			}
		})
	}

	return(
		willRedirect === true
			?
			<Redirect to="/"/>
			:
			<Row className="justify-content-center">
				<Col xs md="6">
					<h2 className="text-center my-4">Register</h2>
					<Card>
						<Form onSubmit={e => registerUser(e)}>
							<Card.Body>

                                <Form.Group controlId="username">
									<Form.Label>Username:</Form.Label>
									<Form.Control type="text" placeholder="Enter your username" value={username} onChange={e => setUsername(e.target.value)} required/>
								</Form.Group>

								<Form.Group controlId="userEmail">
									<Form.Label>Email:</Form.Label>
									<Form.Control type="email" placeholder="Enter your email" value={email} onChange={e => setEmail(e.target.value)} required/>
								</Form.Group>

								<Form.Group controlId="password1">
									<Form.Label>Password:</Form.Label>
									<Form.Control type="password" placeholder="Enter your password" value={password1} onChange={e => setPassword1(e.target.value)} required/>
									<Form.Text className="text-muted">Passwords must be at least 6 characters long.</Form.Text>
								</Form.Group>

								<Form.Group controlId="password2">
									<Form.Label>Verify Password:</Form.Label>
									<Form.Control type="password" placeholder="Verify your password" value={password2} onChange={e => setPassword2(e.target.value)} required/>
								</Form.Group>

							</Card.Body>
							<Card.Footer>
								{isActive === true
									? <Button variant="success" type="submit" block>Register</Button>
									: error === true
										? <Button variant="muted" type="submit" disabled block>Please enter your registration details</Button>
										: <Button variant="muted" type="submit" disabled block>Passwords must match and be at least 6 characters long.</Button>
								}
							</Card.Footer>
						</Form>
					</Card>
					<p className="text-center mt-3">Already have an account? <Link to="/">Click here</Link> to log in.</p>
				</Col>
			</Row>
	)
}
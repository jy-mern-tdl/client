import React, { useState, useEffect, useContext } from 'react';
import { Button, Card, Container, Form, Modal } from 'react-bootstrap'
import LoginForm from '../components/LoginForm';
import moment from 'moment';
import Swal from 'sweetalert2'

import UserContext from '../UserContext';

export default function Tasks(){

    const { user, setUser } = useContext(UserContext)
    const [taskData, setTaskData] = useState([])
	const [taskList, setTaskList] = useState([])
    const [taskName, setTaskName] = useState([])
    const [showAdd, setShowAdd] = useState(false)

    const openAdd = () => setShowAdd(true)
	const closeAdd = () => setShowAdd(false)

    const fetchData = () => {
		fetch(`${ process.env.REACT_APP_API_URL}/tasks/get`, {
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			}
		})
		.then(res => res.json())
		.then(data => {
            console.log(data)
			setTaskData(data)
		})
    }

	useEffect(()=> {
        fetchData()
	}, [])

    useEffect(()=> {
        if(taskData.length > 0){
            const tasks = taskData.map((task, index)=> {
                return(
                    <Card key={task._id}>
                        <Card.Body>
                            <div className='d-flex justify-content-between'>
                                <h5 className='card-title'>Task #{index + 1}</h5>
                                <p className='card-text text-muted'>Made on: {moment(task.timestamp).format("MMMM Do YYYY, h:mm:ss a")}</p>
                            </div>
                            <p className='card-text'>Name: {task.taskName}</p>
                            <p className='card-text'>Complete: {task.isComplete === "true" ? <span>Yes</span> : <span>No</span>}</p>
                        </Card.Body>
                        <Card.Footer>
                            {task.isComplete === "true"
                                ?
                                <Button variant="primary" className="mr-2" onClick={() => {updateTask(task._id, task.taskName, false)}}>Unmark as Done</Button>
                                :
                                <Button variant="success" className="mr-2" onClick={() => {updateTask(task._id, task.taskName, true)}}>Mark as Done</Button>
                            }
                            <Button variant="danger" onClick={() => {deleteTask(task._id)}}>Delete</Button>                                  
                        </Card.Footer>
                    </Card>
                )
            })

            setTaskList(tasks)				
        }else{
            setTaskList([])
        }
    }, [taskData])

	const addTask = (e) => {

		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/tasks/add`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				taskName: taskName,
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				fetchData()
                Swal.fire(
					'Success',
					'Task added.',
					'success'
				)
				setTaskName("")
				closeAdd()
			}else{
				Swal.fire(
					'Error',
					'Something went wrong.',
					'error'
				)
				closeAdd()
			}
		})
	}

    const updateTask = (taskId, taskName, isComplete) => {
        
        fetch(`${ process.env.REACT_APP_API_URL}/tasks/update`, {
			method: 'PUT',
			headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`,
                'Content-Type': 'application/json'
            },
			body: JSON.stringify({
                taskId: taskId,
                taskName: taskName,
                isComplete: isComplete
			})
		})
		.then(res => res.json())
		.then(data => {
            if(data){
				Swal.fire(
					'Success',
					'Task has been updated.',
					'success'
				)
                fetchData();
            }else{
				Swal.fire(
					'Error',
					'Something went wrong.',
					'error'
				)
            }
		})
    }

    const deleteTask = (taskId) => {
        
        fetch(`${ process.env.REACT_APP_API_URL}/tasks/delete`, {
			method: 'DELETE',
			headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`,
                'Content-Type': 'application/json'
            },
			body: JSON.stringify({
                taskId: taskId,
			})
		})
		.then(res => res.json())
		.then(data => {
            if(data){
				Swal.fire(
					'Success',
					'Task has been deleted.',
					'success'
				)
                fetchData();
            }else{
				Swal.fire(
					'Error',
					'Something went wrong.',
					'error'
				)
            }
		})
    }
	return(
		user.id !== null
		?
        <>
            <Container>
                <h3 className="text-center py-3">My To-Do List:</h3>
                <div className="d-flex justify-content-center mb-4">
                        <Button className="mr-1" variant="primary" onClick={openAdd}>Add New Task</Button>
                </div>
                {taskList.length > 0
                    ?
                    <>
                        {taskList}
                    </>
                    :
                    <h3 className="text-center">No tasks yet!</h3>
                }

            </Container>
            <Modal show={showAdd} onHide={closeAdd}>
                <Form onSubmit={e => addTask(e)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add New Task</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                            <Form.Group controlId="taskName">
                                <Form.Label>Name:</Form.Label>
                                <Form.Control type="text" placeholder="Enter task name" value={taskName} onChange={e => setTaskName(e.target.value)} required/>
                            </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={closeAdd}>Close</Button>
                        <Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>
                </Form>	
            </Modal>
        </>
		:
		<LoginForm fetchData={fetchData}/>
	)
}